Source: knot-resolver
Section: net
Priority: optional
Maintainer: knot-resolver packagers <knot-resolver@packages.debian.org>
Uploaders:
 Ondřej Surý <ondrej@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
Build-Depends-Indep:
 doxygen,
 python3-breathe,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Build-Depends:
 debhelper-compat (= 12),
 dns-root-data,
 gnutls-bin <!nocheck>,
 knot-dnsutils <!nocheck>,
 libcmocka-dev (>= 1.0.0),
 libedit-dev,
 libgeoip-dev,
 libgnutls28-dev,
 libknot-dev (>= 2.7.2),
 libknot-dev (<< 2.8.0),
 liblmdb-dev,
 libluajit-5.1-dev,
 libsystemd-dev (>= 227) [linux-any],
 libuv1-dev,
 luajit,
 pkg-config,
 socat <!nocheck>,
Standards-Version: 4.3.0
Homepage: https://www.knot-resolver.cz/
Vcs-Browser: https://salsa.debian.org/dns-team/knot-resolver
Vcs-Git: https://salsa.debian.org/dns-team/knot-resolver.git
Rules-Requires-Root: no

Package: knot-resolver
# intended to be "Architecture: any [!arm64]", see #907729
Architecture: amd64 armel armhf i386 mips mips64el mipsel ppc64 ppc64el
Depends:
 adduser,
 dns-root-data,
 lua-sec,
 lua-socket,
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
  libkres9 (<< 3.2.1-2),
Breaks:
  libkres9 (<< 3.2.1-2),
Recommends:
 knot-resolver-module-http,
 lua-basexx,
 lua-cqueues,
Description: caching, DNSSEC-validating DNS resolver
 The Knot Resolver is a caching full resolver implementation
 written in C and LuaJIT, including both a resolver library and a
 daemon. Modular architecture of the library keeps the core tiny and
 efficient, and provides a state-machine like API for
 extensions. There are three built-in modules - iterator, cache,
 validator, and many external.
 .
 The Lua modules, switchable and shareable cache, and fast FFI
 bindings makes it great to tap into resolution process, or be used
 for your recursive DNS service. It's the OpenResty of DNS.
 .
 The server adopts a different scaling strategy than the rest of the
 DNS recursors - no threading, shared-nothing architecture (except
 MVCC cache that may be shared). You can start and stop additional
 nodes depending on the contention without downtime.

Package: knot-resolver-module-http
Architecture: all
Depends:
 fonts-glyphicons-halflings,
 libjs-bootstrap,
 libjs-d3,
 libjs-jquery,
 lua-http,
 lua-mmdb,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 knot-resolver-module-tinyweb (<< 1.1.0~git20160713-1~),
Description: HTTP/2 module for Knot Resolver
 The Knot Resolver is a caching full resolver implementation
 written in C and LuaJIT, including both a resolver library and a
 daemon. Modular architecture of the library keeps the core tiny and
 efficient, and provides a state-machine like API for
 extensions. There are three built-in modules - iterator, cache,
 validator, and many external.
 .
 This package contains HTTP/2 module for local visualization of the
 resolver cache and queries.

Package: knot-resolver-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-jquery,
 libjs-underscore,
 ${misc:Depends},
Description: Documentation for Knot Resolver
 The Knot Resolver is a caching full resolver implementation
 written in C and LuaJIT, including both a resolver library and a
 daemon. Modular architecture of the library keeps the core tiny and
 efficient, and provides a state-machine like API for
 extensions. There are three built-in modules - iterator, cache,
 validator, and many external.
 .
 This package contains Knot Resolver Documentation.
